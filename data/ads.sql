-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 19 2020 г., 02:36
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ads`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ads`
--

INSERT INTO `ads` (`id`, `brand_id`, `model_id`, `mileage`, `price`, `phone`) VALUES
(1, 1, 1, 234578, 2500000, '89620000000'),
(2, 1, 2, 123745, 2850000, '89620000012'),
(3, 1, 3, NULL, 3460000, '89620000043'),
(4, 2, 4, 83056, 1250000, '89620000087'),
(5, 2, 6, NULL, 1250000, '89620000000'),
(6, 3, 7, 230879, 2500000, '89620000000');

-- --------------------------------------------------------

--
-- Структура таблицы `ads_options`
--

CREATE TABLE `ads_options` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ads_options`
--

INSERT INTO `ads_options` (`id`, `ad_id`, `option_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 3),
(8, 2, 5),
(9, 3, 1),
(10, 5, 1),
(11, 5, 2),
(12, 5, 4),
(13, 6, 1),
(14, 6, 3),
(15, 6, 4),
(16, 6, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(1, 'Audi'),
(2, 'BMW'),
(3, 'Mazda');

-- --------------------------------------------------------

--
-- Структура таблицы `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `images`
--

INSERT INTO `images` (`id`, `ad_id`, `path`, `type`) VALUES
(1, 1, 'uploads/images/cars/ea167de117769d341ad15bddcae563e4.jpg', 'original'),
(2, 1, 'uploads/images/cars/small/ea167de117769d341ad15bddcae563e4.jpg', 'small'),
(3, 1, 'uploads/images/cars/large/ea167de117769d341ad15bddcae563e4.jpg', 'large'),
(4, 1, 'uploads/images/cars/d1e842feb1240f333d94694861f7d9d1.jpg', 'original'),
(5, 1, 'uploads/images/cars/small/d1e842feb1240f333d94694861f7d9d1.jpg', 'small'),
(6, 1, 'uploads/images/cars/large/d1e842feb1240f333d94694861f7d9d1.jpg', 'large'),
(7, 2, 'uploads/images/cars/b6ff1e38d54ec4d7f5df6743abc2d1bb.jpg', 'original'),
(8, 2, 'uploads/images/cars/small/b6ff1e38d54ec4d7f5df6743abc2d1bb.jpg', 'small'),
(9, 2, 'uploads/images/cars/large/b6ff1e38d54ec4d7f5df6743abc2d1bb.jpg', 'large'),
(10, 2, 'uploads/images/cars/4a698e052aac906da9ee11514878c4d6.jpg', 'original'),
(11, 2, 'uploads/images/cars/small/4a698e052aac906da9ee11514878c4d6.jpg', 'small'),
(12, 2, 'uploads/images/cars/large/4a698e052aac906da9ee11514878c4d6.jpg', 'large'),
(13, 5, 'uploads/images/cars/4282edc150f3d63ccc82623166e0cedf.jpeg', 'original'),
(14, 5, 'uploads/images/cars/small/4282edc150f3d63ccc82623166e0cedf.jpeg', 'small'),
(15, 5, 'uploads/images/cars/large/4282edc150f3d63ccc82623166e0cedf.jpeg', 'large'),
(16, 6, 'uploads/images/cars/80dce3ea7494dee93b5a0d78c170b3e4.jpg', 'original'),
(17, 6, 'uploads/images/cars/small/80dce3ea7494dee93b5a0d78c170b3e4.jpg', 'small'),
(18, 6, 'uploads/images/cars/large/80dce3ea7494dee93b5a0d78c170b3e4.jpg', 'large'),
(19, 6, 'uploads/images/cars/6995aadff41acecbccc855f8b5ded8cb.jpg', 'original'),
(20, 6, 'uploads/images/cars/small/6995aadff41acecbccc855f8b5ded8cb.jpg', 'small'),
(21, 6, 'uploads/images/cars/large/6995aadff41acecbccc855f8b5ded8cb.jpg', 'large'),
(22, 6, 'uploads/images/cars/2ee1668b7a7b2aa4cef29802ce06f65a.jpeg', 'original'),
(23, 6, 'uploads/images/cars/small/2ee1668b7a7b2aa4cef29802ce06f65a.jpeg', 'small'),
(24, 6, 'uploads/images/cars/large/2ee1668b7a7b2aa4cef29802ce06f65a.jpeg', 'large'),
(25, 8, 'uploads/images/cars/aa39b79013fa2291537468728dd497f7.jpg', 'original'),
(26, 8, 'uploads/images/cars/small/aa39b79013fa2291537468728dd497f7.jpg', 'small'),
(27, 8, 'uploads/images/cars/large/aa39b79013fa2291537468728dd497f7.jpg', 'large'),
(28, 8, 'uploads/images/cars/7fcbae19aaa354996ec703527ae20b91.jpg', 'original'),
(29, 8, 'uploads/images/cars/small/7fcbae19aaa354996ec703527ae20b91.jpg', 'small'),
(30, 8, 'uploads/images/cars/large/7fcbae19aaa354996ec703527ae20b91.jpg', 'large'),
(31, 8, 'uploads/images/cars/c1bca22200642ec534a46a1cfe54259b.jpg', 'original'),
(32, 8, 'uploads/images/cars/small/c1bca22200642ec534a46a1cfe54259b.jpg', 'small'),
(33, 8, 'uploads/images/cars/large/c1bca22200642ec534a46a1cfe54259b.jpg', 'large');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1579387934),
('m200118_222530_create_ads_table', 1579387937),
('m200118_222643_create_options_table', 1579387938),
('m200118_222651_create_ads_options_table', 1579387938),
('m200118_222714_create_images_table', 1579387938),
('m200118_222732_create_brands_table', 1579387938),
('m200118_222745_create_models_table', 1579387938);

-- --------------------------------------------------------

--
-- Структура таблицы `models`
--

CREATE TABLE `models` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `models`
--

INSERT INTO `models` (`id`, `name`, `brand_id`) VALUES
(1, 'A5', 1),
(2, 'A6', 1),
(3, 'A7', 1),
(4, '5 серия', 2),
(5, '6 серия', 2),
(6, '7 серия', 2),
(7, 'CX 5', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`) VALUES
(1, 'Крепление детского сидения'),
(2, 'Подушка безопасности водителя'),
(3, 'ABS'),
(4, 'Датчик дождя'),
(5, 'Сигнализация');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ads_options`
--
ALTER TABLE `ads_options`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `ads_options`
--
ALTER TABLE `ads_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT для таблицы `models`
--
ALTER TABLE `models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

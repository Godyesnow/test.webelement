<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Автомобили';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('errorMessage')): ?>
        <div class="alert alert-success"><?=Yii::$app->session->getFlash('errorMessage');?></div>
    <?php endif; ?>
    <?php if (count($items) > 0): ?>
        <div class="ad-list">
            <?foreach($items as $item):?>
                <div class="ad-list-item">
                    <img src="/<?=$item->image->path ?? 'uploads/images/no-image-sm.jpg';?>" alt="" class="ad-list-item__img">
                    <div class="ad-list-item__info">
                        <a href="/ads/details?id=<?=$item->id;?>" class="ad-list-item__link">
                            <h3 class="ad-list-item__title"><?=Html::encode($item->brand->name);?> <?=Html::encode($item->model->name);?></h3>
                        </a>
                        <div class="ad-list-item__price">Цена: <span><?=Html::encode($item->price);?></span> руб.</div>
                        <a href="/ads/delete?id=<?=$item->id;?>" class="ad-list-item__delete">Удалить</a>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    <?php else: ?>
        <code>Список пуст, однако именно вы можете стать первым</code>
    <?php endif; ?>
</div>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>

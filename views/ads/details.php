<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $item->brand->name . ' ' . $item->model->name;
$this->params['breadcrumbs'][] = ['url' => '/ads/list', 'label' => 'Автомобили'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

    <div class="ad-details">
        <div class="ad-details__img">
            <div class="ad-details__img-lg">
                <img src="/<?=$item->large->path ?? 'uploads/images/no-image.jpg';?>" alt="<?=$item->large->path;?>">
            </div>
            <div class="ad-details__img-sm">
                <?foreach($item->smalls as $image):?>
                    <img src="/<?=$image->path;?>" alt="<?=$image->path;?>">
                <?endforeach;?>
            </div>
        </div>
        <div class="ad-details__info">
            <h1 class="ad-details__name"><?= Html::encode($this->title) ?></h1>
            <div class="ad-details__prop">
                <span>Марка:</span>
                <span><?= Html::encode($item->brand->name) ?></span>
            </div>
            <div class="ad-details__prop">
                <span>Модель:</span>
                <span><?= Html::encode($item->model->name) ?></span>
            </div>
            <div class="ad-details__prop">
                <span>Цена:</span>
                <span><?= Html::encode($item->price) ?> руб.</span>
            </div>
            <div class="ad-details__prop">
                <span>Телефон:</span>
                <span><?= Html::encode($item->phone) ?></span>
            </div>
            <div class="ad-details__prop">
                <span>Пробег:</span>
                <span><?= Html::encode($item->mileage ?? 0) ?> км.</span>
            </div>
            <?if(isset($item->options) && count($item->options) > 0):?>
                <div class="ad-details__props">
                    <div class="ad-details__props-title">Дополнительное оборудование:</div>
                    <div class="ad-details__props-items">
                        <?foreach($item->options as $opt):?>
                            <span><?=$opt->name;?></span>
                        <?endforeach;?>
                    </div>
                </div>
            <?endif;?>
        </div>
    </div>

</div>

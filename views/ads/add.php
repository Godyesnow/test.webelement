<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Добавление объявления';
$this->params['breadcrumbs'][] = ['url' => '/ads/list', 'label' => 'Автомобили'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('AdAddFormSubmitted')): ?>
        <div class="alert alert-success">Объявление успешно добавленно.</div>
    <?php else: ?>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?$prepare = [0 => 'Выбрать'];?>
                <?foreach($brands as $item):?>
                    <?$prepare[$item->id] = $item->name;?>
                <?endforeach?>
                <?= $form->field($model, 'brand_id')->dropDownList($prepare)->label('Марка'); ?>

                <?= $form->field($model, 'model_id')->dropDownList([
                    0 => 'Выбрать',
                    2 => 'Test'
                ])->label('Модель'); ?>

                <?= $form->field($model, 'mileage')->label('Пробег') ?>

                <?= $form->field($model, 'price')->label('Цена') ?>

                <?= $form->field($model, 'phone')->label('Телефон') ?>

                <?$prepare = [];?>
                <?foreach($options as $item):?>
                    <?$prepare[$item->id] = $item->name;?>
                <?endforeach?>
                <?=
                $form->field($model, 'options')->checkboxList(
                    $prepare
                // ['separator' => '<br>']
                )->label('Дополнительное оборудование')
                ?>

                <?= $form->field($model, 'images[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Фото') ?>

                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>

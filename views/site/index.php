<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

	<div class="jumbotron">
		<h2>Модуль добавления объявлений автомобилей</h2>
		<p><a class="btn btn-lg btn-success" href="/ads/add">Добавить</a></p>
	</div>
	<div class="body-content">
		<div class="row">
			<div class="col-lg-3">
				<h2>Автомобили</h2>
				<p><a class="btn btn-default" href="/ads/list">Перейти &raquo;</a></p>
			</div>
			<div class="col-lg-3">
				<h2>Доп/оборудование</h2>
				<p><a class="btn btn-default" href="/options/list">Перейти &raquo;</a></p>
			</div>
			<div class="col-lg-3">
				<h2>Марки</h2>
				<p><a class="btn btn-default" href="/brands/list">Перейти &raquo;</a></p>
			</div>
			<div class="col-lg-3">
				<h2>Модели</h2>
				<p><a class="btn btn-default" href="/models/list">Перейти &raquo;</a></p>
			</div>
		</div>

	</div>
</div>

<?php


namespace app\models;

use yii\db\ActiveRecord;

class Ad extends ActiveRecord
{
    /**
     * @return string название таблицы, сопоставленной с этим ActiveRecord-классом.
     */
    public static function tableName ()
    {
        return '{{ads}}';
    }

    public function getBrand ()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function getModel ()
    {
        return $this->hasOne(Model::className(), ['id' => 'model_id']);
    }

    public function getOptions ()
    {
        return $this->hasMany(Option::className(), ['id' => 'option_id'])
            ->viaTable('ads_options', ['ad_id' => 'id']);
    }

    public function getImage ()
    {
        return $this->hasOne(Image::className(), ['ad_id' => 'id'])
            ->where(['type' => 'small'])
            ->orderBy(['id' => SORT_ASC])
            /*->limit(1)*/;
    }

    public function getImages ()
    {
        return $this->hasMany(Image::className(), ['ad_id' => 'id'])
            ->where(['type' => 'small']);
    }

    public function getSmalls ()
    {
        return $this->hasMany(Image::className(), ['ad_id' => 'id'])
            ->where(['type' => 'small']);
    }

    public function getLarge ()
    {
        return $this->hasOne(Image::className(), ['ad_id' => 'id'])
            ->where(['type' => 'large'])
            ->orderBy(['id' => SORT_ASC]);
    }
}
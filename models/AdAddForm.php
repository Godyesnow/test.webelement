<?php

namespace app\models;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Yii;
use yii\base\Model;
use yii\imagine\Image;

class AdAddForm extends Model
{
    public $brand_id;
    public $model_id;
    public $mileage;
    public $price;
    public $phone;
    public $images;
    public $options;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['brand_id', 'model_id', 'price', 'phone'], 'required', 'message' => 'Обязательное поле'],
            [['brand_id', 'model_id'], 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number', 'message' => 'Обязательное поле'],
            ['images',
                'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'checkExtensionByMimeType' => true,
                'maxSize' => 2048000,
                'tooBig' => 'Limit is 2MB',
                'maxFiles' => 3,
                'skipOnEmpty' => true
            ],
            ['options', 'each', 'rule' => ['integer'], 'skipOnEmpty' => true],
            ['mileage',
                'string',
                'skipOnEmpty' => true
            ]
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return bool whether the model passes validation
     */
    public function save ()
    {
        if ($this->validate()) {
            $row = new Ad();
            $row->brand_id = $this->brand_id;
            $row->model_id = $this->model_id;
            $row->mileage = $this->mileage;
            $row->price = $this->price;
            $row->phone = $this->phone;
            $row->save();
            $images = [];
            foreach ($this->images as $file) {
                $fileName = md5('image' . time() . rand(1000, 9999));
                $file->saveAs('uploads/images/cars/' . $fileName . '.' . $file->extension);
                $images[] = [
                    $row->id,
                    'uploads/images/cars/' . $fileName . '.' . $file->extension,
                    'original'
                ];
                $this->resize($fileName, $file);
                $images[] = [
                    $row->id,
                    "uploads/images/cars/small/" . $fileName . '.' . $file->extension,
                    'small'
                ];
                $this->resize($fileName, $file, 720, 540, 'large');
                $images[] = [
                    $row->id,
                    "uploads/images/cars/large/" . $fileName . '.' . $file->extension,
                    'large'
                ];
            }

            $options = isset($this->options) && is_array($this->options) ? $this->options: [];

            if (count($options) > 0) {
                $tmp = [];
                foreach ($options as $item)  {
                    $tmp[] = [$row->id, $item];
                }
                Yii::$app->db->createCommand()->batchInsert('ads_options', ['ad_id', 'option_id'], $tmp)->execute();
            }

            Yii::$app->db->createCommand()->batchInsert('images', ['ad_id', 'path', 'type'], $images)->execute();

            return true;
        }
        return false;
    }

    /**
     * ...
     *
     * @return string
     */
    protected function resize ($fileName, $file, $width = 146, $height = 106, $type = 'small')
    {
        $thumbnail = Image::resize('@webroot/uploads/images/cars/' . $fileName . '.' . $file->extension, $width, $height);
        $size = $thumbnail->getSize();
        if ($size->getWidth() < $width or $size->getHeight() < $height) {
            $white = Image::getImagine()->create(new Box($width, $height));
            $thumbnail = $white->paste($thumbnail, new Point($width / 2 - $size->getWidth() / 2, $height / 2 - $size->getHeight() / 2));
        }
        $alias = Yii::getAlias("@webroot/uploads/images/cars/{$type}/" . $fileName . '.' . $file->extension);
        $thumbnail->save($alias, ['quality' => 80]);
        return $alias;
    }
}
<?php

namespace app\models;

use yii\base\Model;
use app\models\Model as ModelObject;

class ModelAddForm extends Model
{
    public $brand_id;
    public $name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Обязательное поле'],
            [['brand_id'], 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number', 'message' => 'Обязательное поле'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return bool whether the model passes validation
     */
    public function save ()
    {
        if ($this->validate()) {
            $row = new ModelObject();
            $row->brand_id = $this->brand_id;
            $row->name = $this->name;
            $row->save();
            return true;
        }
        return false;
    }
}
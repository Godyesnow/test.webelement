<?php

namespace app\models;

use yii\base\Model;

class BrandAddForm extends Model
{
    public $name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Обязательное поле'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return bool whether the model passes validation
     */
    public function save ()
    {
        if ($this->validate()) {
            $row = new Brand();
            $row->name = $this->name;
            $row->save();
            return true;
        }
        return false;
    }
}
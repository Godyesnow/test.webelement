<?php

namespace app\models;

use app\models\Option;
use yii\base\Model;

class OptionAddForm extends Model
{
    public $name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Обязательное поле']
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @return bool whether the model passes validation
     */
    public function save ()
    {
        if ($this->validate()) {
            $row = new Option();
            $row->name = $this->name;
            $row->save();
            return true;
        }
        return false;
    }
}
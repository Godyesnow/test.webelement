<?php


namespace app\controllers;

use app\models\Brand;
use app\models\Model;
use app\models\ModelAddForm;
use Yii;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;

class ModelsController extends Controller
{
    /**
     * Отображает страницу добавления
     *
     * @return string
     */
    public function actionAdd ()
    {
        $this->layout = 'models';
        $brands = Brand::find()->all();
        $model = new ModelAddForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('AdAddFormSubmitted', 'Объявление успешно добавленно');
                return $this->refresh();
            }
        }
        return $this->render('add', [
            'brands' => $brands,
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу списка
     *
     * @return string
     */
    public function actionList ()
    {
        $this->layout = 'models';
        $query = Model::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $items = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('list', [
            'items' => $items,
            'pages' => $pages,
        ]);
    }

    /**
     * Удаление
     *
     * @return string
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete ()
    {
        $request = Yii::$app->request;
        $id = $request->get('id') ?? null;
        if (is_null($id)) { throw new Exception('Необходим идентификатор записи'); }
        $query = new Query;
        $has = $query->select('id')
            ->from('ads')
            ->where(['model_id' => $id]);
        if ($has->exists()) {
            Yii::$app->session->setFlash('errorMessage', 'Сначала удалите связанное объявление');
        } else {
            (Model::findOne($id))->delete();
            Yii::$app->session->setFlash('errorMessage', 'Успешно удалено');
        }
        return $this->redirect('/models/list');
    }
}
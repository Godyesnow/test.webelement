<?php

namespace app\controllers;

use app\models\Option;
use app\models\OptionAddForm;
use Yii;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;

class OptionsController extends Controller
{
    /**
     * Отображает страницу добавления.
     *
     * @return string
     */
    public function actionAdd ()
    {
        $this->layout = 'options';
        $model = new OptionAddForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('OptionAddFormSubmitted', 'Объявление успешно добавленно');
                return $this->refresh();
            }
        }
        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу списка.
     *
     * @return string
     */
    public function actionList ()
    {
        $this->layout = 'options';
        $query = Option::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $items = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('list', [
            'items' => $items,
            'pages' => $pages,
        ]);
    }

    /**
     * Отображает страницу редактирования.
     *
     * @return string
     */
    public function actionUpdate ()
    {
        try {
            $model = new OptionAddForm();
            if ($model->load(Yii::$app->request->post())) {
                $model->update = true;
                if ($model->save()) {
                    Yii::$app->session->setFlash('OptionEditFormSubmitted', 'Объявление успешно добавленно');
                    return $this->refresh();
                }
            } else {
                $request = Yii::$app->request;
                $id = $request->get('id');
                $item = Option::findOne($id);
                $this->layout = 'ads';
                $model->name = $item->name;
                return $this->render('edit', [
                    'item' => $item,
                    'model' => $model,
                ]);
            }
        } catch (\Exception $e) {
            exit('exception - ' . $e->getMessage());
        }
    }

    /**
     * Удаление
     *
     * @return string
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throw string
     */
    public function actionDelete ()
    {
        $request = Yii::$app->request;
        $id = $request->get('id') ?? null;
        if (is_null($id)) { throw new Exception('error mx'); }
        $query = new Query;
        $has = $query->select('id')
            ->from('ads_options')
            ->where(['option_id' => $id])
            ->exists();
        if ($has) {
            Yii::$app->session->setFlash('errorMessage', 'Сначала удалите связанное объявление.');
            return $this->redirect('/options/list');
        } else {
            (Option::findOne($id))->delete();
            Yii::$app->session->setFlash('errorMessage', 'Успешно удалено.');
        }
        return $this->redirect('/options/list');
    }
}
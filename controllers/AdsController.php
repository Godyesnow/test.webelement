<?php

namespace app\controllers;

use app\models\AdAddForm;
use app\models\Ad;
use app\models\Option;
use app\models\Brand;
use Yii;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;
use yii\web\UploadedFile;

class AdsController extends Controller
{
    /**
     * Отображает список объявлений
     *
     * @return string
     */
    public function actionList ()
    {
        $this->layout = 'ads';
        $query = Ad::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $items = $query
            ->with('image')
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('list', [
            'items' => $items,
            'pages' => $pages,
        ]);
    }

    /**
     * Отображает детали объевления.
     *
     * @return string
     */
    public function actionDetails ()
    {
        try {
            $request = Yii::$app->request;
            $this->layout = 'ads';
            $id = $request->get('id');
            $item = Ad::find()
                ->with('brand')
                ->with('model')
                ->with('smalls')
                ->with('large')
                ->with('options')
                ->where(['id' => $id])
                ->one();
            if (!$item) { throw new Exception('Not founded'); }
            return $this->render('details', ['item' => $item]);
        } catch (\Exception $e) {
            exit('exception - ' . $e->getMessage());
        }
    }

    /**
     * Отображает страницу добавления нового объявления.
     *
     * @return string
     */
    public function actionAdd ()
    {
        $this->layout = 'ads';
        $options = Option::find()->all();
        $brands = Brand::find()->all();
        $model = new AdAddForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->images = UploadedFile::getInstances($model, 'images');
            if ($model->save()) {
                Yii::$app->session->setFlash('AdAddFormSubmitted', 'Объявление успешно добавленно');
                return $this->refresh();
            }
        }
        return $this->render('add', [
            'options' => $options,
            'brands' => $brands,
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу добавления нового объявления.
     *
     * @return string
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete ()
    {
        $request = Yii::$app->request;
        $id = $request->get('id') ?? null;
        if (is_null($id)) { throw new Exception('error mx'); }
        $query = new Query;
        $query->select('id')
            ->from('ads_options')
            ->where(['ad_id' => $id]);
        if ($query->exists()) {
            Yii::$app->db->createCommand()->delete('ads_options', 'ad_id = ' . $id)->execute();
        }
        $row = Ad::findOne($id);
        $row->delete();
        Yii::$app->session->setFlash('errorMessage', 'Успешно удалено.');
        return $this->redirect('/ads/list');
    }
}

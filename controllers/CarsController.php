<?php

namespace app\controllers;

use app\models\Model;
use Yii;
use yii\web\Controller;

class CarsController extends Controller
{
    /**
     * ...
     *
     * @return array
     */
    public function actionModels ()
    {
        try {
            $request = Yii::$app->request;
            $id = $request->get('id');
            $items = Model::find()->where(['brand_id' => $id])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'success' => true,
                'data' => $items,
            ];
        } catch (\Exception $e) {
            exit('exception - ' . $e->getMessage());
        }
    }
}

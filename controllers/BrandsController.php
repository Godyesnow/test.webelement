<?php

namespace app\controllers;

use app\models\BrandAddForm;
use app\models\Brand;
use Yii;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Query;
use yii\web\Controller;

class BrandsController extends Controller
{
    /**
     * Отображает страницу добавления нового объявления.
     *
     * @return string
     */
    public function actionAdd ()
    {
        $this->layout = 'brands';
        $model = new BrandAddForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('brandAddFormSubmitted', 'Успешно добавленн');
                return $this->refresh();
            }
        }
        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу списка.
     *
     * @return string
     */
    public function actionList ()
    {
        $this->layout = 'brands';
        $query = Brand::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $items = $query
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['id' => SORT_DESC])
            ->all();

        return $this->render('list', [
            'items' => $items,
            'pages' => $pages,
        ]);
    }

    /**
     * Удаление
     *
     * @return string
     * @throws Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throw string
     */
    public function actionDelete ()
    {
        $request = Yii::$app->request;
        $id = $request->get('id') ?? null;
        if (is_null($id)) { throw new Exception('Необходим идентификатор записи'); }
        $query = new Query;
        $hasAd = $query->select('id')
            ->from('ads')
            ->where(['brand_id' => $id]);
        $query2 = new Query;
        $hasModel = $query2->select('id')
            ->from('models')
            ->where(['brand_id' => $id]);
        if ($hasAd->exists() || $hasModel->exists()) {
            Yii::$app->session->setFlash('errorMessage', 'Сначала удалите связанное объявление или модели авто');
        } else {
            (Brand::findOne($id))->delete();
            Yii::$app->session->setFlash('errorMessage', 'Успешно удалено.');
        }
        return $this->redirect('/brands/list');
    }
}
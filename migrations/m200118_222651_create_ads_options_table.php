<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ads_options}}`.
 */
class m200118_222651_create_ads_options_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ads_options}}', [
            'id' => $this->primaryKey(),
            'ad_id' => $this->integer(),
            'option_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ads_options}}');
    }
}

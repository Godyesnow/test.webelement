'use strict';

window.addEventListener('DOMContentLoaded', function (e) {
    try {
        const fromSelect = document.getElementById('adaddform-brand_id');
        const toSelect = document.getElementById('adaddform-model_id');
        fromSelect.addEventListener('change', function (e) {
            const id = e.target.options[e.target.selectedIndex].value;
            if (id > 0) {
                console.log(id);
                fetch('/cars/models?id=' + id).then(function (res) {
                    if (res.status === 200) {
                        res.json().then(function (res) {
                            if (res.success) {
                                let arr = res.data;
                                arr.unshift({id: 0, name: 'Выбрать'});
                                toSelect.innerHTML = '';
                                for (let item of arr) {
                                    let option = document.createElement('option');
                                    option.value = item.id;
                                    option.textContent = item.name;
                                    toSelect.appendChild(option);
                                }
                            }
                        });
                    }
                }).catch(function (err) {
                    console.log('err', err);
                });
            } else {
                toSelect.innerHTML = '';
                let option = document.createElement('option');
                option.value = 0;
                option.textContent = 'Выбрать';
                toSelect.appendChild(option);
            }
        });
    } catch (e) {}
});